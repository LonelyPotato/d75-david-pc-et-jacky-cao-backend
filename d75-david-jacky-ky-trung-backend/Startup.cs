﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using d75_david_jacky_ky_trung_backend.Helpers;

[assembly: OwinStartup(typeof(d75_david_jacky_ky_trung_backend.Startup))]

namespace d75_david_jacky_ky_trung_backend
{
    public class Startup
    {
        //Venant du projet SDP
        public void ConfigureAuth(IAppBuilder app)
        {
            // nous creeons notre configuration pour l'authentification
            OAuthAuthorizationServerOptions OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                // pour definir une URL pour pouvoir acceder au Token
                TokenEndpointPath = new PathString("/api/Token"),
                // pour definir le temps de persistance du token
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(60),
                // pour definir l'instance qui va verifier et dire que l'utilisateur est valide
                Provider = new SimpleAuthorizationServerProvider()
            };

            // ici nous disons que a chaque nouvelle requete le serveur
            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            ConfigureAuth(app);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}