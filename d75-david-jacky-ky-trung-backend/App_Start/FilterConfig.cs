﻿using System.Web;
using System.Web.Mvc;

namespace d75_david_jacky_ky_trung_backend
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
