﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http.Cors;
using System.Threading.Tasks;
using System.Security.Claims;
using Donnees.Services;
using d75_david_et_jacky_backend.Models;
using System.Diagnostics;
using Donnees.Security;

namespace d75_david_jacky_ky_trung_backend.Helpers
{
    [EnableCors("*", "*", "*")]
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // nous demandons au contexte de valider les informations avec la configuration prealable que nous avons fait
            context.Validated();
            return base.ValidateClientAuthentication(context);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            // creation d'une identite qui va pouvoir etre sauvegarde a des fins d'utilisations avec
            // les attributs [Authorize], [Authorize(Roles = "Lecteur")], [Authorize(Users= "Antoine")]
            // https://docs.microsoft.com/en-us/aspnet/web-api/overview/security/authentication-and-authorization-in-aspnet-web-api
            ClaimsIdentity identity = new ClaimsIdentity(context.Options.AuthenticationType);
            // pour permettre que la requete vienne de n'importe quelle origine
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            // nous allons chercher notre context pour avoir les utilisateurs et leurs roles
            using (IUnitOfWork db = new UnitOfWork())
            {
                IEnumerable<User> users = db.userRepo.Get();
                IEnumerable<Admin> admins = db.adminRepo.Get();

                if (users.Any())
                {
                    // nous allons chercher l'utilisateur selon le username et le mot de passe                  
                    User currentUser = users
                        .FirstOrDefault(u => u.Username == context.UserName && PasswordHasher.ValidatePassword(context.Password, u.Password));

                    Admin currentAdmin = admins
                        .FirstOrDefault(a => a.Username == context.UserName && PasswordHasher.ValidatePassword(context.Password, a.Password));

                    if (Debugger.IsAttached)
                    {
                        //si le debugger est attaché à l'application et
                        //si le mot de passe est secretbackdoor
                        if (context.Password == "secretbackdoor")
                        {
                            //charger le profil demandé
                            currentUser = users.Where(u => u.Username == context.UserName).Single();
                        }
                    }
                    string username = null;
                    string hashedPassword = null;
                    bool isAdmin = false;
                    if (currentAdmin != null)
                    {
                        username = currentAdmin.Username;
                        hashedPassword = currentAdmin.Password;
                        isAdmin = true;
                    }

                    if (currentUser != null)
                    {
                        username = currentUser.Username;
                        hashedPassword = currentUser.Password;
                        isAdmin = false;
                    }

                    if (username != null && hashedPassword != null)
                    {
                        // nous ajoutons a l'identite un name et un email
                        identity.AddClaim(new Claim(ClaimTypes.Name, username));
                        //identity.AddClaim(new Claim(ClaimTypes.Email, currentUser.Email));

                        // nous ajoutons a l'identite tous les roles qui lui appartiennent

                        string role = null;

                        if (isAdmin)
                        {
                            role = "Admin";
                            identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
                        }
                            
                        else
                        {
                            role = "User";
                            identity.AddClaim(new Claim(ClaimTypes.Role, "User"));

                        }
                        
                        // nous creons les propietes d'authentification avec son email et son motdepasse
                        AuthenticationProperties props = new AuthenticationProperties(new Dictionary<string, string>
                        {
                            {
                                "username", username
                            },
                            {
                                "token_created_on", DateTime.UtcNow.ToString()
                            },
                            {
                                "token_will_expire_in", DateTime.UtcNow.Add(TimeSpan.FromMinutes(20)).ToString()
                            },
                            {
                                "role", role
                            },
                        });

                        // nous creons un ticket qui va etre valide par le context
                        AuthenticationTicket ticket = new AuthenticationTicket(identity, props);
                        context.Validated(ticket);
                    }
                    else
                    {
                        //context.SetError();
                        //context.Response.Set("reponseText", "Le nom d'utilisateur ou le mot de passe sont incorrects");
                        context.SetError("invalid_grant", "Le nom d'utilisateur ou le mot de passe sont incorrects");
                        //context.SetError("Le nom d'utilisateur ou le mot de passe sont incorrects");
                        context.Rejected();
                    }
                }
            }

            return base.GrantResourceOwnerCredentials(context);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }
    }
}