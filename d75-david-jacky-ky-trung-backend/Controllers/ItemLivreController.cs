﻿using d75_david_et_jacky_backend.Models;
using Donnees.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;

namespace d75_david_et_jacky_backend.Controllers
{
    public class ItemLivreController : ApiController
    {
        private IUnitOfWork DB;
        private ItemLivreService service;

        #region Constructeurs

        public ItemLivreController()
        {
            DB = new UnitOfWork();
            service = new ItemLivreService(DB);
        }

        public ItemLivreController(IUnitOfWork uow)
        {
            DB = uow;
            service = new ItemLivreService(uow);
        }

        #endregion

        #region GET Methods

        [HttpGet]
        [ResponseType(typeof(ItemLivre))]
        [Route("api/ItemLivre/{id}")]
        public IHttpActionResult GetItemLivreById(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                ItemLivre itemLivre = service.GetParId(id);

                if (itemLivre == null)
                    return NotFound();

                return Ok(itemLivre);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ItemLivre>))]
        [Route("api/ItemLivres")]
        public IHttpActionResult GetAllItemLivres()
        {
            try
            {
                IEnumerable<ItemLivre> allItemLivres = service.GetAll();

                return Ok(allItemLivres);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ItemLivre>))]
        [Route("api/ItemLivreByVendeurId/{id}")]
        public IHttpActionResult GetItemLivreByVendeur(int id)
        {
            if (id < 1)
                return BadRequest("Vendeur invalide");

            try
            {
                IEnumerable<ItemLivre> allItemLivres = service.GetAllParIdVendeur(id);

                if (allItemLivres == null)
                    return NotFound();

                return Ok(allItemLivres);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<ItemLivre>))]
        [Route("api/ItemLivresParIdLivre/{id}")]
        public IHttpActionResult GetAllItemLivresParIdLivre(int id)
        {
            try
            {
                IEnumerable<ItemLivre> itemLivres = service.GetAllParIdLivre(id);

                return Ok(itemLivres);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region PUT Methods

        [HttpPut]
        [ResponseType(typeof(ItemLivre))]
        [Route("api/ItemLivre/{id}")]
        public IHttpActionResult UpdateItemLivre(int id, ItemLivre newItemLivre)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                ItemLivre itemLivre = service.GetParId(id);

                if (itemLivre == null)
                    return NotFound();

                itemLivre.Titre = newItemLivre.Titre;
                itemLivre.ImageURLs = newItemLivre.ImageURLs;
                itemLivre.Etat = newItemLivre.Etat;
                itemLivre.Prix = newItemLivre.Prix;
                itemLivre.Description = newItemLivre.Description;
                itemLivre.Statut = newItemLivre.Statut;
                //maybe add ImageURLs

                service.Update(itemLivre);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(itemLivre);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region POST Methods

        [HttpPost]
        [ResponseType(typeof(ItemLivre))]
        [Route("api/ItemLivre")]
        public IHttpActionResult NewItemLivre(ItemLivre itemLivre)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");

            try
            {
                service.Add(itemLivre);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(itemLivre);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/ItemLivreImages")]
        public IHttpActionResult Images(HttpPostedFileBase file)
        {
            if (file == null)
                return BadRequest("Le fichier est null");

            try
            {
                file.SaveAs(HostingEnvironment.MapPath($"/Images/ImagesItemLivres/lalalalal.png"));
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        #endregion

        #region DELETE Methods

        [HttpDelete]
        [ResponseType(typeof(ItemLivre))]
        [Route("api/ItemLivre/{id}")]
        public IHttpActionResult DeleteItemLivre(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                ItemLivre itemLivre = service.GetParId(id);

                if (itemLivre == null)
                    return NotFound();

                service.Delete(itemLivre.Id);
                int retour = service.Save();

                if (retour > 0)
                    return Ok();
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion
    }
}
