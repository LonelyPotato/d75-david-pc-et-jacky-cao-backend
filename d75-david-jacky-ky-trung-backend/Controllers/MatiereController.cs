﻿using d75_david_et_jacky_backend.Models;
using Donnees.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace d75_david_et_jacky_backend.Controllers
{
    public class MatiereController : ApiController
    {
        private IUnitOfWork DB;
        private MatiereService service;

        #region Constructeurs

        public MatiereController()
        {
            DB = new UnitOfWork();
            service = new MatiereService(DB);
        }

        public MatiereController(IUnitOfWork uow)
        {
            DB = uow;
            service = new MatiereService(uow);
        }

        #endregion

        #region GET Methods

        [HttpGet]
        [ResponseType(typeof(Matiere))]
        [Route("api/Matiere/{id}")]
        public IHttpActionResult GetMatiereById(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Matiere matiere = service.GetParId(id);

                if (matiere == null)
                    return NotFound();

                return Ok(matiere);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<Matiere>))]
        [Route("api/Matieres")]
        public IHttpActionResult GetAllMatieres()
        {
            try
            {
                IEnumerable<Matiere> allMatieres = service.GetAll();

                return Ok(allMatieres);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region PUT Methods

        [HttpPut]
        [ResponseType(typeof(Matiere))]
        [Route("api/Matiere/{id}")]
        public IHttpActionResult UpdateMatiere(int id, Matiere newMatiere)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Matiere matiere = service.GetParId(id);

                if (matiere == null)
                    return NotFound();

                matiere.Nom = newMatiere.Nom;
                service.Update(matiere);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(matiere);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region POST Methods

        [HttpPost]
        [ResponseType(typeof(Matiere))]
        [Route("api/Matiere")]
        public IHttpActionResult NewMatiere(Matiere matiere)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");

            try
            {
                service.Add(matiere);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(matiere);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region DELETE Methods

        [HttpDelete]
        [ResponseType(typeof(Matiere))]
        [Route("api/Matiere/{id}")]
        public IHttpActionResult DeleteMatiere(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Matiere matiere = service.GetParId(id);

                if (matiere == null)
                    return NotFound();

                service.Delete(matiere.Id);
                int retour = service.Save();

                if (retour > 0)
                    return Ok();
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

    }
}
