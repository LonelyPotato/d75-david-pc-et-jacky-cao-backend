﻿using d75_david_et_jacky_backend.Models;
using Donnees.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace d75_david_et_jacky_backend.Controllers
{
    public class LivreController : ApiController
    {
        private IUnitOfWork DB;
        private LivreService service;

        #region Constructeurs

        public LivreController()
        {
            DB = new UnitOfWork();
            service = new LivreService(DB);
        }

        public LivreController(IUnitOfWork uow)
        {
            DB = uow;
            service = new LivreService(uow);
        }

        #endregion

        #region GET Methods

        [HttpGet]
        [ResponseType(typeof(Livre))]
        [Route("api/Livre/{id}")]
        public IHttpActionResult GetLivreById(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Livre livre = service.GetParId(id);

                if (livre == null)
                    return NotFound();

                return Ok(livre);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<Livre>))]
        [Route("api/Livres")]
        public IHttpActionResult GetAllLivres()
        {
            try
            {
                IEnumerable<Livre> allLivres = service.GetAll();

                return Ok(allLivres);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<Livre>))]
        [Route("api/LivresParIdMatiere/{id}")]
        public IHttpActionResult GetAllLivresParIdMatiere(int id)
        {
            try
            {
                IEnumerable<Livre> livres = service.GetAllParIdMatiere(id);

                return Ok(livres);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region PUT Methods

        [HttpPut]
        [ResponseType(typeof(Livre))]
        [Route("api/Livre/{id}")]
        public IHttpActionResult UpdateLivre(int id, Livre newLivre)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Livre livre = service.GetParId(id);

                if (livre == null)
                    return NotFound();

                livre.Titre = newLivre.Titre;
                livre.ImageURL = newLivre.ImageURL;
                livre.Edition = newLivre.Edition;
                livre.Auteur = newLivre.Auteur;
                livre.IdMatiere = newLivre.IdMatiere;
                service.Update(livre);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(livre);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region POST Methods

        [HttpPost]
        [ResponseType(typeof(Livre))]
        [Route("api/Livre")]
        public IHttpActionResult NewLivre(Livre livre)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");

            try
            {
                service.Add(livre);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(livre);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region DELETE Methods

        [HttpDelete]
        [ResponseType(typeof(Livre))]
        [Route("api/Livre/{id}")]
        public IHttpActionResult DeleteLivre(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Livre livre = service.GetParId(id);

                if (livre == null)
                    return NotFound();

                service.Delete(livre.Id);
                int retour = service.Save();

                if (retour > 0)
                    return Ok();
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion
    }
}
