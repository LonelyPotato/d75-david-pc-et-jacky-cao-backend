﻿using d75_david_et_jacky_backend.Models;
using Donnees.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace d75_david_jacky_ky_trung_backend.Controllers
{
    public class AdminController : ApiController
    {
        private IUnitOfWork DB;
        private AdminService service;

        #region Constructeurs

        public AdminController()
        {
            DB = new UnitOfWork();
            service = new AdminService(DB);
        }

        public AdminController(IUnitOfWork uow)
        {
            DB = uow;
            service = new AdminService(uow);
        }

        #endregion

        #region GET Methods

        [HttpGet]
        [ResponseType(typeof(Admin))]
        [Route("api/Admin/{id}")]
        public IHttpActionResult GetAdminById(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Admin admin = service.GetParId(id);

                if (admin == null)
                    return NotFound();

                return Ok(admin);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<Admin>))]
        [Route("api/Admins")]
        public IHttpActionResult GetAllAdmins()
        {
            try
            {
                IEnumerable<Admin> allAdmins = service.GetAll();

                return Ok(allAdmins);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region PUT Methods

        [HttpPut]
        [ResponseType(typeof(Admin))]
        [Route("api/Admin/{id}")]
        public IHttpActionResult UpdateAdmin(int id, Admin newAdmin)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Admin admin = service.GetParId(id);

                if (admin == null)
                    return NotFound();

                admin.Username = newAdmin.Username;
                admin.Password = newAdmin.Password;
                service.Update(admin);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(admin);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region POST Methods

        [HttpPost]
        [ResponseType(typeof(Admin))]
        [Route("api/Admin")]
        public IHttpActionResult NewAdmin(Admin admin)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");

            try
            {
                service.Add(admin);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(admin);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region DELETE Methods

        [HttpDelete]
        [ResponseType(typeof(Admin))]
        [Route("api/Admin/{id}")]
        public IHttpActionResult DeleteAdmin(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Admin admin = service.GetParId(id);

                if (admin == null)
                    return NotFound();

                service.Delete(admin.Id);
                int retour = service.Save();

                if (retour > 0)
                    return Ok();
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion
    }
}
