﻿using d75_david_et_jacky_backend.Models;
using Donnees.Security;
using Donnees.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace d75_david_et_jacky_backend.Controllers
{
    public class UserController : ApiController
    {
        private IUnitOfWork DB;
        private UserService service;
        private AdminService adService;

        #region Constructeurs

        public UserController()
        {
            DB = new UnitOfWork();
            service = new UserService(DB);
            adService = new AdminService(DB);
        }

        public UserController(IUnitOfWork uow)
        {
            DB = uow;
            service = new UserService(uow);
            adService = new AdminService(uow);
        }

        #endregion

        #region GET Methods

        [HttpGet]
        [ResponseType(typeof(User))]
        [Route("api/User/{id}")]
        public IHttpActionResult GetUserById(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                User user = service.GetParId(id);

                if (user == null)
                    return NotFound();

                return Ok(user);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(User))]
        [Route("api/UserByEmail/{email}")]
        public IHttpActionResult GetUserByEmail(string email)
        {
            if (email.Length <= 0)
                return BadRequest("Veuillez entrer un email");

            try
            {
                User user = service.GetUserParEmail(email);

                if (user == null)
                    return NotFound();
                return Ok(user);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(User))]
        [Route("api/UserByUsername/{username}")]
        public IHttpActionResult GetUserByUsername(string username)
        {
            if (username.Length <= 0)
                return BadRequest("Veuillez entrer un username");

            try
            {
                User user = service.GetUserParUsername(username);
                if (user == null) {

                    Admin admin = adService.GetAdminParUsername(username);
                    if(admin != null)
                        return Ok(admin);

                    return NotFound();
                }

                return Ok(user);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<User>))]
        [Route("api/Users")]
        public IHttpActionResult GetAllUsers()
        {
            try
            {
                IEnumerable<User> allUsers =  service.GetAll();

                return Ok(allUsers);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        #endregion

        #region PUT Methods

        [HttpPut]
        [ResponseType(typeof(User))]
        [Route("api/User/{id}")]
        public IHttpActionResult UpdateUser(int id, User newUser)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                User user = service.GetParId(id);

                if (user == null)
                    return NotFound();

                user.Nom = newUser.Nom;
                user.Prenom = newUser.Prenom;
                user.Password = PasswordHasher.CreateHash(newUser.Password);
                user.Email = newUser.Email;
                service.Update(user);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(user);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region POST Methods

        [HttpPost]
        [ResponseType(typeof(User))]
        [Route("api/User")]
        public IHttpActionResult NewUser(User user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");

            try
            {
                user.Password = PasswordHasher.CreateHash(user.Password);
                service.Add(user);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(user);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region DELETE Methods

        [HttpDelete]
        [ResponseType(typeof(User))]
        [Route("api/User/{id}")]
        public IHttpActionResult DeleteUser(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                User user = service.GetParId(id);

                if (user == null)
                    return NotFound();

                service.Delete(user.Id);
                int retour = service.Save();

                if (retour > 0)
                    return Ok();
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion
    }
}
