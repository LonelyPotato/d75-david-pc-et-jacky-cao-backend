﻿using d75_david_et_jacky_backend.Models;
using Donnees.DAL;
using Donnees.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace d75_david_jacky_ky_trung_backend.Controllers
{
    public class CommandeController : ApiController
    {
        private IUnitOfWork DB;
        private CommandeService service;

        #region Constructeurs

        public CommandeController()
        {
            DB = new UnitOfWork();
            service = new CommandeService(DB);
        }

        public CommandeController(IUnitOfWork uow)
        {
            DB = uow;
            service = new CommandeService(uow);
        }

        #endregion

        #region GET Methods

        [HttpGet]
        [ResponseType(typeof(Commande))]
        [Route("api/Commande/{id}")]
        public IHttpActionResult GetCommandeById(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Commande commande = service.GetParId(id);

                if (commande == null)
                    return NotFound();

                return Ok(commande);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(Commande))]
        [Route("api/CommandeParNoCommande/{noCommande}")]
        public IHttpActionResult GetCommandeByNoCommande(string noCommande)
        {
            if (noCommande == null)
                return BadRequest("No. Commande invalide");

            try
            {
                Commande commande = service.GetParNoCommande(noCommande);

                if (commande == null)
                    return NotFound();

                return Ok(commande);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<Commande>))]
        [Route("api/CommandeByClient/{username}")]
        public IHttpActionResult GetCommandesByClient(string username)
        {
            if (username.Length <= 0)
                return BadRequest("Username invalide");

            try
            {
                IEnumerable<Commande> allCommandes = service.GetAllParUsername(username);

                if (allCommandes == null)
                    return NotFound();

                return Ok(allCommandes);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<Commande>))]
        [Route("api/Commandes")]
        public IHttpActionResult GetAllCommandes()
        {
            try
            {
                IEnumerable<Commande> allCommandes = service.GetAll();

                return Ok(allCommandes);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        
        #endregion

        #region POST Methods

        [HttpPost]
        [ResponseType(typeof(Commande))]
        [Route("api/Commande")]
        public IHttpActionResult NewCommande(Commande commande)
        {
            if (!ModelState.IsValid)
                return BadRequest("Le modèle est invalide");

            try
            {
                
                commande.NumeroCommande = generateRandomOrderNumber(8);

                service.Add(commande);
                int retour = service.Save();

                if (retour > 0)
                    return Ok(commande);
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        public string generateRandomOrderNumber(int length)
        {
            Random random = new Random();
            
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }
        #endregion

        #region DELETE Methods

        [HttpDelete]
        [ResponseType(typeof(Commande))]
        [Route("api/Commande/{id}")]
        public IHttpActionResult DeleteCommande(int id)
        {
            if (id < 1)
                return BadRequest("ID invalide: le ID doit être plus grand que 0");

            try
            {
                Commande commande = service.GetParId(id);

                if (commande == null)
                    return NotFound();

                service.Delete(commande.Id);
                int retour = service.Save();

                if (retour > 0)
                    return Ok();
                else
                    return this.BadRequest("Problème de contraintes sur la BD");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion
    }
}