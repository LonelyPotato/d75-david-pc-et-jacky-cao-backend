using System;
using System.Reflection;

namespace d75_david_jacky_ky_trung_backend.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}