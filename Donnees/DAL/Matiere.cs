using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace d75_david_et_jacky_backend.Models
{
    public class Matiere
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Un nom est requis", AllowEmptyStrings = false)]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Le nom de la matière devrait être entre 3 et 30 caractères")]
        public string Nom { get; set; }

        public virtual ICollection<Livre> Livres { get; set; }
    }
}