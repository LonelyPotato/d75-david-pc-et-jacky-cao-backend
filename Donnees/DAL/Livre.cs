using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace d75_david_et_jacky_backend.Models
{
    public class Livre
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Un titre est requis", AllowEmptyStrings = false)]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Le titre devrait être entre 3 et 100 caractères")]
        public string Titre { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageURL { get; set; }

        [Required(ErrorMessage = "Une édition est requise", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "L'édition devrait être entre 3 et 50 caractères")]
        public string Edition { get; set; }

        [Required(ErrorMessage = "Un auteur est requis", AllowEmptyStrings = false)]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Le nom du ou des auteur(s) devrait être entre 3 et 100 caractères")]
        public string Auteur { get; set; }

        public virtual ICollection<ItemLivre> ItemLivres { get; set; }

        [ForeignKey("IdMatiere")]
        public virtual Matiere Matiere { get; set; }

        [ForeignKey("Matiere")]
        public int? IdMatiere { get; set; }

    }
}