using Donnees.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace d75_david_et_jacky_backend.Models
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Un nom est requis", AllowEmptyStrings = false)]
        [StringLength (50, MinimumLength = 3, ErrorMessage = "Le nom devrait être entre 3 et 50 caractères")]
        public string Nom { get; set; }

        [Required(ErrorMessage = "Un prénom est requis", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Le prénom devrait être entre 3 et 50 caractères")]
        public string Prenom { get; set; }

        [Required(ErrorMessage = "Un nom d'utilisateur est requis", AllowEmptyStrings = false)]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Le nom d'utilisateur devrait être entre 3 et 20 caractères")]
        [Index(IsUnique = true)]
        public string Username { get; set; }

        [Required(ErrorMessage = "Un mot de passe est requis", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Un courriel est requis")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } 

        public virtual ICollection<ItemLivre> ItemLivres { get; set; }

        public virtual ICollection<Commande> Commandes { get; set; }

    }
}