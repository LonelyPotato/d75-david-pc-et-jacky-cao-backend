﻿using d75_david_et_jacky_backend.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.DAL
{
    public class VenteLivreDb : DbContext
    {
        public VenteLivreDb() : base("VenteLivreConnectionString")
        {
            Database.SetInitializer(new InitDatabaseItems());
            this.Configuration.LazyLoadingEnabled = false;
        }

        public VenteLivreDb(DbConnection connection) : base(connection, true)
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Livre> Livres { get; set; }
        public DbSet<ItemLivre> ItemLivres { get; set; }
        public DbSet<Matiere> Matieres { get; set; }
        public DbSet<Commande> Commandes { get; set; }
    }
}
