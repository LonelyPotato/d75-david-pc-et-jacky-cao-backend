﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace d75_david_et_jacky_backend.Models
{
    public class Admin
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Un nom d'utilisateur est requis", AllowEmptyStrings = false)]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Le nom d'utilisateur devrait être entre 3 et 20 caractères")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Un mot de passe est requis", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}