﻿using d75_david_et_jacky_backend.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.DAL
{
    public class Commande
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(8)]
        [Index(IsUnique = true)]
        public string NumeroCommande { get; set; }

        [ForeignKey("IdItemAchete")]
        public virtual ItemLivre ItemAchete { get; set; }
        [ForeignKey("ItemAchete")]
        public int? IdItemAchete { get; set; }
        [ForeignKey("IdClient")]
        public virtual User Client { get; set; }
        [ForeignKey("Client")]
        public int IdClient { get; set; }
    }
}
