﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Linq;
using System.Web;

namespace d75_david_et_jacky_backend.Models
{
    public enum Etat
    {
        Excellent, TresBon, Bon, Acceptable
    }

    public enum Statut
    {
        Libre, Reserve, Achete, ArgentRecupere
    }

    public class ItemLivre
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Un titre est requis", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Le titre devrait être entre 3 et 50 caractères")]
        public string Titre { get; set; }

        public string ImageURLs { get; set; }

        [Required(ErrorMessage = "Un état est requis")]
        public Etat Etat { get; set; }

        [Required(ErrorMessage = "Un prix est requis")]
        [DataType(DataType.Currency)]
        public double Prix { get; set; }

        [StringLength(2000, ErrorMessage = "La description ne peut pas dépasser 2000 caractères")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Un statut est requis")]
        public Statut Statut { get; set; }

        [Required(ErrorMessage = "Une date de mise envente est requise")]
        [DataType(DataType.Date)]
        public DateTime DateMiseEnVente { get; set; }

        [ForeignKey("IdLivre")]
        public virtual Livre Livre { get; set; }

        [ForeignKey("Livre")]
        public int? IdLivre { get; set; }

        [ForeignKey("IdVendeur")]
        public virtual User Vendeur { get; set; }

        [ForeignKey("Vendeur")]
        public int IdVendeur { get; set; }
    }
}