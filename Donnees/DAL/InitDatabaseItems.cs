using d75_david_et_jacky_backend.Models;
using Donnees.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.DAL
{
    class InitDatabaseItems : DropCreateDatabaseIfModelChanges<VenteLivreDb>
    {
        public InitDatabaseItems() : base() { }

        protected override void Seed(VenteLivreDb context)
        {
            context.Admins.AddOrUpdate(x => x.Id,
                new Admin() { Id = 1, Username = "Admin1", Password = PasswordHasher.CreateHash("Admin1") });

            context.Commandes.AddOrUpdate(x => x.Id,
                new Commande() { Id = 1, NumeroCommande = "TEST1234", IdItemAchete = 5, IdClient = 7 },
                new Commande() { Id = 2, NumeroCommande = "TEST6543", IdItemAchete = 3, IdClient = 2 },
                new Commande() { Id = 3, NumeroCommande = "TEST1245", IdItemAchete = 8, IdClient = 9 },
                new Commande() { Id = 4, NumeroCommande = "TEST6532", IdItemAchete = 7, IdClient = 11 }
                );

            context.Users.AddOrUpdate(x => x.Id,
                new User() { Id = 1, Prenom = "Stephen", Nom = "King", Email = "stephenking@hotmail.com", Username = "StephenKing123", Password = PasswordHasher.CreateHash("StephenKing123") },
                new User() { Id = 2, Prenom = "Junpei", Nom = "Number5", Email = "number5@999.com", Username = "Junpei5", Password = PasswordHasher.CreateHash("Junpei5") },
                new User() { Id = 3, Prenom = "Ligma", Nom = "What", Email = "ligmaseed@gottem.dab", Username = "LigmaDoctor", Password = PasswordHasher.CreateHash("LigmaDoctor") },
                new User() { Id = 4, Prenom = "The Chrome", Nom = "from Fire EMbl", Email = "thechromefromfireembl@lacuna.com", Username = "Chrome", Password = PasswordHasher.CreateHash("Chrome") },
                new User() { Id = 5, Prenom = "Thomas", Nom = "Desjardins", Email = "thomasdesjardins@hotmail.com", Username = "xXxTheKillerxXx", Password = PasswordHasher.CreateHash("xXxTheKillerxXx") },
                new User() { Id = 6, Prenom = "Henry", Nom = "Gray", Email = "henrygray@outlook.com", Username = "Gray", Password = PasswordHasher.CreateHash("Gray") },
                new User() { Id = 7, Prenom = "Emma", Nom = "Lapierre", Email = "emmalapierre@yahoo.com", Username = "TheRock", Password = PasswordHasher.CreateHash("TheRock") },
                new User() { Id = 8, Prenom = "Charles", Nom = "Dubois", Email = "charlesdubois@gmail.com", Username = "Wood", Password = PasswordHasher.CreateHash("Wood") },
                new User() { Id = 9, Prenom = "Tony", Nom = "Tiger", Email = "tonytiger@cereals.com", Username = "FrostedFlakes", Password = PasswordHasher.CreateHash("FrostedFlakes") },
                new User() { Id = 10, Prenom = "Hakim", Nom = "Maloof", Email = "hakimmaloof@hotmail.com", Username = "MalOOF", Password = PasswordHasher.CreateHash("MalOOF") },
                new User() { Id = 11, Prenom = "Julie", Nom = "Jolie", Email = "juliejolie@gmail.com", Username = "juliejolie1999", Password = PasswordHasher.CreateHash("juliejolie1999") },
                new User() { Id = 12, Prenom = "Margaret", Nom = "Stewart", Email = "margaretstewart@hotmail.com", Username = "Margarine", Password = PasswordHasher.CreateHash("Margarine") },
                new User() { Id = 13, Prenom = "Jonathan", Nom = "Joestar", Email = "jonathanjoestar@jojo.com", Username = "JoJo1", Password = PasswordHasher.CreateHash("JoJo1") },
                new User() { Id = 14, Prenom = "Cynthia", Nom = "Davis", Email = "cynthiadavis@hotmail.com", Username = "Cycy", Password = PasswordHasher.CreateHash("Cycy") }
                );

            context.Matieres.AddOrUpdate(x => x.Id,
                new Matiere() { Id = 1, Nom = "Francais" },
                new Matiere() { Id = 2, Nom = "Mathematiques" },
                new Matiere() { Id = 3, Nom = "Informatique" },
                new Matiere() { Id = 4, Nom = "Anglais" },
                new Matiere() { Id = 5, Nom = "Espagnol" },
                new Matiere() { Id = 6, Nom = "Philosophie" },
                new Matiere() { Id = 7, Nom = "Education Physique" },
                new Matiere() { Id = 8, Nom = "Administration" }
                );

            context.Livres.AddOrUpdate(x => x.Id,
                new Livre()
                {
                    Id = 1,
                    Titre = "L'entreprise",
                    Auteur = "Dominique Lamaute, Julie Laganière, Hugues Chassé, Insaf Ben Kadhi",
                    Edition = "Chenelière Éducation",
                    IdMatiere = 8,
                    ImageURL = "https://www.cheneliere.ca/DATA/ITEM/9258.jpg"
                },
                new Livre()
                {
                    Id = 2,
                    Titre = "Méthodes de design UX",
                    Auteur = "Carine Lallemand, Guillaume Gronier",
                    Edition = "Eyrolles",
                    IdMatiere = 3,
                    ImageURL = "https://images-na.ssl-images-amazon.com/images/I/41Fgc52K8AL._SX258_BO1,204,203,200_.jpg"
                },
                new Livre()
                {
                    Id = 3,
                    Titre = "Android Programming: The Big Nerd Ranch Guide",
                    Auteur = "Bill Phillips, Chris Stewart, Kristin Marsicano",
                    Edition = "Big Nerd Ranch Guides",
                    IdMatiere = 3,
                    ImageURL = "https://images-na.ssl-images-amazon.com/images/I/412puBdQscL._SX353_BO1,204,203,200_.jpg"
                },
                new Livre()
                {
                    Id = 4,
                    Titre = "Le dernier jour d'un condamné",
                    Auteur = "Victor Hugo",
                    Edition = "Le Livre de Poche",
                    IdMatiere = 1,
                    ImageURL = "https://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/3/6/0/9782253050063/tsp20121001174052/Le-dernier-jour-d-un-condamne.jpg"
                },
                new Livre()
                {
                    Id = 5,
                    Titre = "Le libraire",
                    Auteur = "Gérard Bessette",
                    Edition = "Pierre Tisseyre",
                    IdMatiere = 1,
                    ImageURL = "https://img.src.ca/2015/04/28/469x703/150428_rr532_aetd-gerard-bessette_dt469.jpg"
                },
                new Livre()
                {
                    Id = 6,
                    Titre = "Le français au collège",
                    Auteur = "Damien Jay",
                    Edition = "ellipses",
                    IdMatiere = 1,
                    ImageURL = "https://www.editions-ellipses.fr/images/9782340011724.jpg"
                },
                new Livre()
                {
                    Id = 7,
                    Titre = "Algèbre linéaire et géométrie vectorielle, 5e édition",
                    Auteur = "Gilles Charron, Pierre Parent",
                    Edition = "Chenelière Éducation",
                    IdMatiere = 2,
                    ImageURL = "https://www.coopuqam.com/DATA/ITEM/grande/591719~v~Algebre_lineaire_et_geometrie_vectorielle_5e_edition.jpg"
                },
                new Livre()
                {
                    Id = 8,
                    Titre = "Vente",
                    Auteur = "Fernando Marin, Reyes Morales",
                    Edition = "edelsa",
                    IdMatiere = 5,
                    ImageURL = "https://pictures.abebooks.com/SERLOGAL2012/21909005714.jpg"
                },
                new Livre()
                {
                    Id = 9,
                    Titre = "Apologie de Socrate - Criton",
                    Auteur = "Platon",
                    Edition = "Garnier Flammarion",
                    IdMatiere = 6,
                    ImageURL = "http://www.renaud-bray.com/ImagesEditeurs/PG/4/4857-gf.jpg"
                },
                new Livre()
                {
                    Id = 10,
                    Titre = "Le badminton doublement simplifié",
                    Auteur = "Sylvain Lehoux",
                    Edition = "ERPI",
                    IdMatiere = 7,
                    ImageURL = "https://images-na.ssl-images-amazon.com/images/I/51h08wrBpCL._SX258_BO1,204,203,200_.jpg"
                }
                );

            context.ItemLivres.AddOrUpdate(x => x.Id,
                new ItemLivre()
                {
                    Id = 1,
                    Titre = "LENTREPRISE A VENDRE PAS CHER",
                    Description = "Voici une description",
                    Statut = Statut.Libre,
                    Etat = Etat.Bon,
                    Prix = 21.00,
                    IdLivre = 1,
                    IdVendeur = 2,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 2,
                    Titre = "L'entreprise pour cours de sys info",
                    Description = "Pour le cours de systeme d'informations",
                    Statut = Statut.Libre,
                    Etat = Etat.Excellent,
                    Prix = 25.00,
                    IdLivre = 1,
                    IdVendeur = 6,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 3,
                    Titre = "Méthodes de design UX pas cher",
                    Description = "Quelques pages sont pliées, certaines ont un peu de surligneur dedans.",
                    Statut = Statut.Libre,
                    Etat = Etat.Acceptable,
                    Prix = 22.00,
                    IdLivre = 2,
                    IdVendeur = 1,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 4,
                    Titre = "BADMINTON DOUBLEMENT SIMPLIFIÉ PAS CHER",
                    Description = "Traces d'écriture dans le livre, mais elles ont été effacées",
                    Statut = Statut.Libre,
                    Etat = Etat.Bon,
                    Prix = 18.00,
                    IdLivre = 10,
                    IdVendeur = 2,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 5,
                    Titre = "Vente - Espagnol",
                    Description = "J'ai utilisé le code internet mais je n'ai pas écrit dans le livre",
                    Statut = Statut.Libre,
                    Etat = Etat.Excellent,
                    Prix = 20.00,
                    IdLivre = 8,
                    IdVendeur = 4,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 6,
                    Titre = "lacuna's book",
                    Description = "dis is my girl lacuna's book and i wanna sell it cause she is no longer a child",
                    Statut = Statut.Libre,
                    Etat = Etat.TresBon,
                    Prix = 16.00,
                    IdLivre = 4,
                    IdVendeur = 4,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 7,
                    Titre = "Livre de programmation Android",
                    Description = "Très bon livre tutoriel pour les développeurs android en parfait état",
                    Statut = Statut.Libre,
                    Etat = Etat.Excellent,
                    Prix = 31.00,
                    IdLivre = 3,
                    IdVendeur = 10,
                    DateMiseEnVente = DateTime.Now,
                },
                new ItemLivre()
                {
                    Id = 8,
                    Titre = "Platon",
                    Description = "Rien a signaler",
                    Statut = Statut.Libre,
                    Etat = Etat.TresBon,
                    Prix = 5.00,
                    IdLivre = 9,
                    IdVendeur = 13,
                    DateMiseEnVente = DateTime.Now,
                },
                new ItemLivre()
                {
                    Id = 9,
                    Titre = "Design UX",
                    Description = "Il y a une page pliée, mais sinon intact",
                    Statut = Statut.Libre,
                    Etat = Etat.Excellent,
                    Prix = 40.00,
                    IdLivre = 2,
                    IdVendeur = 9,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 10,
                    Titre = "Cahier d'exercices pour le cours d'espagnol 1",
                    Description = "Les coins sont retroussés",
                    Statut = Statut.Libre,
                    Etat = Etat.Bon,
                    Prix = 12.00,
                    IdLivre = 8,
                    IdVendeur = 10,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 11,
                    Titre = "Cahier d'exercices français",
                    Description = "Petite tâche de café sur 2-3 pages, mais sinon intact.",
                    Statut = Statut.Libre,
                    Etat = Etat.TresBon,
                    Prix = 16.50,
                    IdLivre = 6,
                    IdVendeur = 5,
                    DateMiseEnVente = DateTime.Now
                },
                new ItemLivre()
                {
                    Id = 12,
                    Titre = "Le Libraire",
                    Description = "N'a même pas été lu une fois au complet, comme neuf.",
                    Statut = Statut.Libre,
                    Etat = Etat.Excellent,
                    Prix = 11.00,
                    IdLivre = 5,
                    IdVendeur = 8,
                    DateMiseEnVente = DateTime.Now
                }
                );

            context.SaveChanges();
        }
    }
}
