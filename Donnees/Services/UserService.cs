﻿using d75_david_et_jacky_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class UserService : GenericService<User>
    {
        public UserService() { }

        public UserService(IUnitOfWork uow) : base(uow) { }

        #region GET Methods

        public User GetUserParEmail(string email)
        {
            return repo.Get(u => u.Email == email).FirstOrDefault();
        }
        public User GetUserParUsername(string username)
        {
            return repo.Get(u => u.Username == username).FirstOrDefault();
        }

        #endregion
    }
}
