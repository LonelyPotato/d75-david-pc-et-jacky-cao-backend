﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public abstract class GenericService<T> : IDisposable where T : class
    {
        protected IUnitOfWork uow;
        protected IGenericRepository<T> repo;

        #region Constructeurs
        public GenericService()
        {
            uow = new UnitOfWork();
            repo = uow.GetRepository<T>();
        }

        public GenericService(IUnitOfWork uow)
        {
            this.uow = uow;
            repo = uow.GetRepository<T>();
        }
        #endregion

        public virtual T GetParId(int id)
        {
            return repo.GetById(id);
        }

        public IEnumerable<T> GetAll()
        {
            return repo.Get();
        }

        public virtual void Update(T entite)
        {
            repo.Update(entite);
        }

        public virtual void Add(T entite)
        {
            repo.Add(entite);
        }

        public virtual void Delete(int id)
        {
            repo.Delete(id);
        }

        public int Save()
        {
            //return repo.Save();
            return uow.Save();
        }

        public void Dispose()
        {
            uow.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
