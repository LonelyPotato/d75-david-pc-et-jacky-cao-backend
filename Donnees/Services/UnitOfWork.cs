﻿using d75_david_et_jacky_backend.Models;
using Donnees.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private VenteLivreDb context;
        //private ICollection<IGenericRepository<T>> repositories;
        public IGenericRepository<User> userRepo { get; private set; }
        public IGenericRepository<Admin> adminRepo { get; private set; }
        public IGenericRepository<Livre> livreRepo { get; private set; }
        public IGenericRepository<ItemLivre> itemLivreRepo { get; private set; }
        public IGenericRepository<Matiere> matiereRepo { get; private set; }
        public IGenericRepository<Commande> commandeRepo { get; private set; }


        public UnitOfWork()
        {
            context = new VenteLivreDb();
            InstancierRepositories(); //Not sure
        }

        public UnitOfWork(VenteLivreDb context)
        {
            this.context = context;
            InstancierRepositories();
        }

        public void InstancierRepositories()
        {
            userRepo = new GenericRepository<User>(context);
            adminRepo = new GenericRepository<Admin>(context);
            livreRepo = new GenericRepository<Livre>(context);
            itemLivreRepo = new GenericRepository<ItemLivre>(context);
            matiereRepo = new GenericRepository<Matiere>(context);
            commandeRepo = new GenericRepository<Commande>(context);
        }


        public IGenericRepository<T> GetRepository<T>() where T : class //PLS CHANGE IFs FOR SOMETHING ELSE (or not)
        {
            if (typeof(T) == typeof(IGenericRepository<User>))
                return userRepo as IGenericRepository<T>;
            else if (typeof(T) == typeof(IGenericRepository<Admin>))
                return adminRepo as IGenericRepository<T>;
            else if (typeof(T) == typeof(IGenericRepository<Livre>))
                return livreRepo as IGenericRepository<T>;
            else if (typeof(T) == typeof(IGenericRepository<ItemLivre>))
                return itemLivreRepo as IGenericRepository<T>;
            else if (typeof(T) == typeof(IGenericRepository<Matiere>))
                return matiereRepo as IGenericRepository<T>;
            else if (typeof(T) == typeof(IGenericRepository<Commande>))
                return commandeRepo as IGenericRepository<T>;
            else
                return new GenericRepository<T>(context);
        }

        public IGenericRepository<User> UserRepository
        {
            get
            {
                if (this.userRepo == null)
                    this.userRepo = new GenericRepository<User>(context);
                return userRepo;
            }
        }

        public IGenericRepository<Admin> AdminRepository
        {
            get
            {
                if (this.adminRepo == null)
                    this.adminRepo = new GenericRepository<Admin>(context);
                return adminRepo;
            }
        }

        public IGenericRepository<Livre> LivreRepository
        {
            get
            {
                if (this.livreRepo == null)
                    this.livreRepo = new GenericRepository<Livre>(context);
                return livreRepo;
            }
        }

        public IGenericRepository<ItemLivre> ItemLivreRepository
        {
            get
            {
                if (this.itemLivreRepo == null)
                    this.itemLivreRepo = new GenericRepository<ItemLivre>(context);
                return itemLivreRepo;
            }
        }

        public IGenericRepository<Matiere> MatiereRepository
        {
            get
            {
                if (this.matiereRepo == null)
                    this.matiereRepo = new GenericRepository<Matiere>(context);
                return matiereRepo;
            }
        }

        public IGenericRepository<Commande> CommandeRepository
        {
            get
            {
                if (this.commandeRepo == null)
                    this.commandeRepo = new GenericRepository<Commande>(context);
                return commandeRepo;
            }
        }

        public int Save()
        {
            int result = context.SaveChanges();
            return result;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
