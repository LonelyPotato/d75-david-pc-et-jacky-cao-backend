﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> Get(
             Expression<Func<T, bool>> filter = null,
             Func<IQueryable<T>, IOrderedQueryable<T>> order = null,
             string includes = "");

        T GetById(int id);
        void Add(T entite);
        void Delete(int id);
        void Update(T entite);

        //int Save();
        //T SingleOrDefault(Expression<Func<T, bool>> filter);
    }
}
