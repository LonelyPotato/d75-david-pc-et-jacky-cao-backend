﻿using d75_david_et_jacky_backend.Models;
using Donnees.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class MatiereRepository : IDisposable
    {
        private VenteLivreDb context;

        public MatiereRepository(VenteLivreDb context)
        {
            this.context = context;
        }

        //GET all
        public IEnumerable<Matiere> GetMatieres()
        {
            return context.Matieres.ToList();
        }

        //GET
        public Matiere GetMatiereByID(int id)
        {
            return context.Matieres.Find(id);
        }

        //POST
        public void AddMatiere(Matiere matiere)
        {
            context.Matieres.Add(matiere);
        }

        //DELETE
        public void DeleteMatiere(int matiereID)
        {
            Matiere matiere = context.Matieres.Find(matiereID);
            context.Matieres.Remove(matiere);
        }

        //PUT
        public void UpdateMatiere(Matiere matiere)
        {
            context.Entry(matiere).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
