﻿using d75_david_et_jacky_backend.Models;
using Donnees.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class CommandeService : GenericService<Commande>
    {
        IGenericRepository<User> repoUser;

        public CommandeService() {
            repoUser = uow.GetRepository<User>();
        }

        public CommandeService(IUnitOfWork uow) : base(uow) {
            repoUser = uow.GetRepository<User>();
        }

        public IEnumerable<Commande> GetAllParUsername(string username)
        {
            return repo.Get(c => c.Client.Username == username);
        }

        public Commande GetParNoCommande(string noCommande)
        {
            return repo.Get(c => c.NumeroCommande == noCommande).FirstOrDefault();
        }
    }
}
