﻿using Donnees.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        internal VenteLivreDb context;
        internal DbSet<T> dbSet;

        public GenericRepository(VenteLivreDb context)
        {
            this.context = context;
            this.dbSet = context.Set<T>();
        }

        //GET all (can be filtered or ordered)
        public virtual IEnumerable<T> Get(
             Expression<Func<T, bool>> filter = null,
             Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
             string includeProperties = "")
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        //GET all
        public IEnumerable<T> Get() => dbSet.ToList();

        //GET
        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        //POST
        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }

        //DELETE (with id)
        public virtual void Delete(int id)
        {
            T entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        //DELETE (with object)
        public virtual void Delete(T entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        //PUT
        public virtual void Update(T entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        //public int Save() => context.SaveChanges();
    }
}
