﻿using d75_david_et_jacky_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class ItemLivreService : GenericService<ItemLivre>
    {
        IGenericRepository<Livre> repoLivre;

        public ItemLivreService() {
            repoLivre = uow.GetRepository<Livre>();
        }

        public ItemLivreService(IUnitOfWork uow) : base(uow) {
            repoLivre = uow.GetRepository<Livre>();
        }

        public IEnumerable<ItemLivre> GetAllParIdLivre(int idLivre)
        {
            return repo.Get(l => l.IdLivre == idLivre);
        }

        public IEnumerable<ItemLivre> GetAllParIdVendeur(int id)
        {
            return repo.Get(l => l.IdVendeur == id);
        }

    }
}
