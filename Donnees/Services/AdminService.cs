﻿using d75_david_et_jacky_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class AdminService : GenericService<Admin>
    {
        public AdminService() { }

        public AdminService(IUnitOfWork uow) : base(uow) { }

        public Admin GetAdminParUsername(string username)
        {
            return repo.Get(a => a.Username == username).FirstOrDefault();
        }
    }
}
