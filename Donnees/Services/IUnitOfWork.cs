﻿using d75_david_et_jacky_backend.Models;
using Donnees.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<T> GetRepository<T>() where T : class;
        IGenericRepository<User> userRepo { get; }
        IGenericRepository<Admin> adminRepo { get; }
        IGenericRepository<Livre> livreRepo { get; }
        IGenericRepository<ItemLivre> itemLivreRepo { get; }
        IGenericRepository<Matiere> matiereRepo { get; }
        IGenericRepository<Commande> commandeRepo { get; }

        int Save();
    }
}
