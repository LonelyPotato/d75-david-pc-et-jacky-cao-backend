﻿using d75_david_et_jacky_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class LivreService : GenericService<Livre>
    {
        IGenericRepository<Matiere> repoMatiere;

        public LivreService()
        {
            repoMatiere = uow.GetRepository<Matiere>();
        }

        public LivreService(IUnitOfWork uow) : base(uow)
        {
            repoMatiere = uow.GetRepository<Matiere>();
        }

        #region GET Methods

        public IEnumerable<Livre> GetAllParIdMatiere(int idMatiere)
        {
            return repo.Get(l => l.IdMatiere == idMatiere);
        }

        #endregion
    }
}
