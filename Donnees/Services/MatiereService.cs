﻿using d75_david_et_jacky_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donnees.Services
{
    public class MatiereService : GenericService<Matiere>
    {
        public MatiereService() { }

        public MatiereService(IUnitOfWork uow) : base(uow) { }
    }
}
