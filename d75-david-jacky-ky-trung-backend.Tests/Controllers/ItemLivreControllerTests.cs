using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using d75_david_et_jacky_backend.Controllers;
using d75_david_et_jacky_backend.Models;
using Donnees.Services;
using Donnees.DAL;
using Effort;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;

namespace d75_david_jacky_ky_trung_backend.Tests.Controllers
{
    [TestClass]
    public class ItemLivreControllerTests
    {
        private VenteLivreDb context;
        private IUnitOfWork db;
        private ItemLivreController target;

        [TestInitialize]
        public void TestInitialize()
        {
            var connection = DbConnectionFactory.CreateTransient();
            context = new VenteLivreDb(connection);
            db = new UnitOfWork(context);

            target = new ItemLivreController(db);

            db.userRepo.Add(new User
            {
                Id = 1,
                Nom = "Jeorge",
                Prenom = "Test",
                Username = "SuperTest",
                Password = "123456",
                Email = "arrowman@hotmail.com",
                ItemLivres = new List<ItemLivre>()
            });

            db.userRepo.Add(new User
            {
                Id = 2,
                Nom = "oof",
                Prenom = "very",
                Username = "usernamecool",
                Password = "23134",
                Email = "veryoof@hotmail.com",
                ItemLivres = new List<ItemLivre>()
            });

            db.matiereRepo.Add(new Matiere
            {
                Id = 1,
                Nom = "TestMatiere",
                Livres = new List<Livre>()
            });

            db.livreRepo.Add(new Livre
            {
                Id = 1,
                Titre = "TestTitre",
                ImageURL = "www.placeholder.com",
                Edition = "Edition LaFontaine",
                Auteur = "TestAuteur",
                ItemLivres = new List<ItemLivre>(),
                IdMatiere = 1
            });

            db.livreRepo.Add(new Livre
            {
                Id = 2,
                Titre = "TestTitre2",
                ImageURL = "22.png",
                Edition = "Edition Test",
                Auteur = "TestAuteurAAA",
                ItemLivres = new List<ItemLivre>(),
                IdMatiere = 1
            });

            db.itemLivreRepo.Add(new ItemLivre {
                Id = 1,
                Titre = "TitreItem1",
                ImageURLs = "",
                Etat = Etat.Bon,
                Prix = 22.50,
                Description = "C'est test",
                Statut = Statut.Libre,
                IdLivre = 1,
                IdVendeur = 1
            });

            db.itemLivreRepo.Add(new ItemLivre
            {
                Id = 2,
                Titre = "TitreItem2",
                ImageURLs = "",
                Etat = Etat.Acceptable,
                Prix = 455.50,
                Description = "C'est test",
                Statut = Statut.Libre,
                IdLivre = 1,
                IdVendeur = 1
            });

            db.itemLivreRepo.Add(new ItemLivre
            {
                Id = 3,
                Titre = "TitreOffre3",
                ImageURLs = "",
                Etat = Etat.Bon,
                Prix = 10.00,
                Description = "Description d'une offre",
                Statut = Statut.Libre,
                IdLivre = 2,
                IdVendeur = 2
            });

            db.Save();
        }

        [TestMethod]
        public void GetItemLivreById_IdValide_RetourneIdLivre()
        {
            // Arrange
            int id = 1;
            ItemLivre itemLivreOriginal = db.itemLivreRepo.GetById(id);
            // Act
            IHttpActionResult response = target.GetItemLivreById(id);
            var result = response as OkNegotiatedContentResult<ItemLivre>;
            ItemLivre itemLivre = result.Content;

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<ItemLivre>));
            Assert.AreEqual(itemLivreOriginal.Id, itemLivre.Id);
        }

        [TestMethod]
        public void GetItemLivreByVendeur_IdValide_RetourneIdItemLivre()
        {
            //Arrange
            int idVendeur = 2;
            List<ItemLivre> listeItemLivres = new List<ItemLivre>();

            foreach (ItemLivre item in context.ItemLivres.Where(x => x.IdVendeur == idVendeur))
                listeItemLivres.Add(item);

            //Act
            IHttpActionResult response = target.GetItemLivreByVendeur(idVendeur);

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<IEnumerable<ItemLivre>>));

            var result = response as OkNegotiatedContentResult<IEnumerable<ItemLivre>>;
            List<ItemLivre> content = result.Content.ToList();

            for (int i = 0; i < content.Count(); i++)
                Assert.AreEqual(content[i].Id, listeItemLivres[i].Id);
        }

        [TestMethod]
        public void GetAllItemLivres_Appelee_RetourneListeItemLivre()
        {
            // Arrange
            int originalCount = db.itemLivreRepo.Get().Count();

            // Act
            IHttpActionResult response = target.GetAllItemLivres();

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<IEnumerable<ItemLivre>>));
            var result = response as OkNegotiatedContentResult<IEnumerable<ItemLivre>>;
            IEnumerable<ItemLivre> allItemLivres = result.Content;
            Assert.AreEqual(allItemLivres.Count(), originalCount);
        }

        [TestMethod]
        public void UpdateItemLivre_ModificationTitreEtat_TitreEtatModifie()
        {
            // Arrange
            int id = 1;
            ItemLivre newItemLivre = db.itemLivreRepo.GetById(id);
            string titreOriginal = newItemLivre.Titre;
            Etat etatOriginal = newItemLivre.Etat;

            newItemLivre.Titre = "NotTitre";
            newItemLivre.Etat = Etat.Acceptable;

            // Act
            IHttpActionResult response = target.UpdateItemLivre(id, newItemLivre);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<ItemLivre>));
            var result = response as OkNegotiatedContentResult<ItemLivre>;
            ItemLivre content = result.Content;
            Assert.AreEqual(id, content.Id);
            Assert.AreNotEqual(titreOriginal, content.Titre);
            Assert.AreNotEqual(etatOriginal, content.Etat);
        }

        [TestMethod]
        public void NewItemLivre_ItemLivreValide_RetourneIdItemLivreAjoute()
        {
            // Arrange
            ItemLivre itemLivre = new ItemLivre
            {
                Id = 4,
                Titre = "TitreItem5",
                ImageURLs = "",
                Etat = Etat.TresBon,
                Prix = 55.50,
                Description = "C'est test",
                Statut = Statut.Libre,
                IdLivre = 1,
                IdVendeur = 1
            };

            // Act
            IHttpActionResult response = target.NewItemLivre(itemLivre);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<ItemLivre>));
            Assert.AreEqual(itemLivre, db.itemLivreRepo.GetById(4));
        }

        [TestMethod]
        public void DeleteItemLivre_IdItemLivreExistant_RetourneNull()
        {
            // Arrange
            int id = 1;

            // Act
            IHttpActionResult response = target.DeleteItemLivre(id);

            // Assert
            Assert.IsNull(db.itemLivreRepo.GetById(id));
        }
    }
}
