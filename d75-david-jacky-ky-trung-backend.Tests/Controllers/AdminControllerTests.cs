using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using d75_david_et_jacky_backend.Models;
using d75_david_jacky_ky_trung_backend.Controllers;
using Effort;
using Donnees.DAL;
using Donnees.Services;
using d75_david_et_jacky_backend.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;
using System.Collections.Generic;

namespace d75_david_jacky_ky_trung_backend.Tests.Controllers
{
    [TestClass]
    public class AdminControllerTests
    {
        private VenteLivreDb context;
        private IUnitOfWork db;
        private AdminController target;

        [TestInitialize]
        public void TestInitialize()
        {
            var connection = DbConnectionFactory.CreateTransient();
            context = new VenteLivreDb(connection);
            db = new UnitOfWork(context);

            target = new AdminController(db);

            db.adminRepo.Add(new Admin
            {
                Id = 1,
                Username = "SuperAdmin21",
                Password = "Incorrect"
            });

            db.adminRepo.Add(new Admin
            {
                Id = 2,
                Username = "AdminCoolandGood",
                Password = "Hello"
            });

            db.Save();
        }

        [TestMethod]
        public void GetAdminById_IdValide_RetourneIdAdmin()
        {
            // Arrange
            int id = 1;
            Admin adminOriginal = db.adminRepo.GetById(id);

            // Act
            IHttpActionResult response = target.GetAdminById(id);
            var result = response as OkNegotiatedContentResult<Admin>;
            Admin admin = result.Content;

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Admin>));
            Assert.AreEqual(adminOriginal.Id, admin.Id);
        }

        [TestMethod]
        public void GetAllAdmins_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            int originalCount = db.adminRepo.Get().Count();

            // Act
            IHttpActionResult response = target.GetAllAdmins();

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<IEnumerable<Admin>>));
            var result = response as OkNegotiatedContentResult<IEnumerable<Admin>>;
            IEnumerable<Admin> allAdmins = result.Content;
            Assert.AreEqual(allAdmins.Count(), originalCount);
        }

        [TestMethod]
        public void UpdateAdmin_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            int id = 1;
            Admin adminOriginal = db.adminRepo.GetById(id);
            string passwordOriginal = adminOriginal.Password;

            adminOriginal.Password = "SuperSecret";
            // Act
            IHttpActionResult response = target.UpdateAdmin(id, adminOriginal);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Admin>));
            var result = response as OkNegotiatedContentResult<Admin>;
            Admin content = result.Content;

            Assert.AreEqual(id, content.Id);
            Assert.AreNotSame(adminOriginal.Password, passwordOriginal);
        }

        [TestMethod]
        public void NewAdmin_AdminValide_RetourneIdAdminAjoute()
        {
            // Arrange
            Admin admin = new Admin
            {
                Id = 3,
                Username = "SuperAdmin22",
                Password = "Incorrect"
            };

            // Act
            IHttpActionResult response = target.NewAdmin(admin);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Admin>));
            Assert.AreEqual(admin, db.adminRepo.GetById(3));
        }

        [TestMethod]
        public void DeleteAdmin_IdAdminExistant_RetourneIdNull()
        {
            // Arrange
            int id = 1;

            // Act
            IHttpActionResult response = target.DeleteAdmin(id);

            // Assert
            Assert.IsNull(db.adminRepo.GetById(id));
        }
    }
}
