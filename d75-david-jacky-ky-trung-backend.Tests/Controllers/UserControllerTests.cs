using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using d75_david_et_jacky_backend.Controllers;
using d75_david_et_jacky_backend.Models;
using Donnees.Services;
using Donnees.DAL;
using Effort;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Linq;

namespace d75_david_jacky_ky_trung_backend.Tests.Controllers
{
    [TestClass]
    public class UserControllerTests
    {
        private VenteLivreDb context;
        private IUnitOfWork db;
        private UserController target;

        [TestInitialize]
        public void TestInitialize()
        {
            var connection = DbConnectionFactory.CreateTransient();
            context = new VenteLivreDb(connection);
            db = new UnitOfWork(context);

            target = new UserController(db);

            db.userRepo.Add(new User
            {
                Id = 1,
                Nom = "Jeorge",
                Prenom = "Test",
                Username = "SuperTest",
                Password = "123456",
                Email = "arrowman@hotmail.com",
                ItemLivres = new List<ItemLivre>()
            });

            db.Save();
        }

        [TestMethod]
        public void GetUserById_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            int id = 1;
            User userOriginal = db.userRepo.GetById(id);

            // Act
            IHttpActionResult response = target.GetUserById(id);
            var result = response as OkNegotiatedContentResult<User>;
            User user = result.Content;
            
            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<User>));
            Assert.AreEqual(userOriginal.Id, user.Id);
        }

        [TestMethod]
        public void GetAllUsers_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            int originalCount = db.userRepo.Get().Count();

            // Act
            IHttpActionResult response = target.GetAllUsers();

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<IEnumerable<User>>));
            var result = response as OkNegotiatedContentResult<IEnumerable<User>>;
            IEnumerable<User> allUsers = result.Content;
            Assert.AreEqual(allUsers.Count(), originalCount);
        }

        [TestMethod]
        public void UpdateUser_IdValide_RetourneIdUser()
        {
            // Arrange
            int id = 1;
            User userOriginal = db.userRepo.GetById(id);
            string nomOriginal = userOriginal.Nom;
            userOriginal.Nom = "NotEvenJeorge";

            // Act
            IHttpActionResult response = target.UpdateUser(id, userOriginal);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<User>));
            var result = response as OkNegotiatedContentResult<User>;
            User content = result.Content;

            Assert.AreEqual(id, content.Id);
            Assert.AreNotEqual(userOriginal.Nom, nomOriginal);
        }

        [TestMethod]
        public void NewUser_UserValide_RetourneIdUserAjoute()
        {
            // Arrange
            User user = new User
            {
                Nom = "NewMan",
                Prenom = "Test",
                Username = "SuperTest",
                Password = "123456",
                Email = "man@hotmail.com",
                ItemLivres = new List<ItemLivre>()
            };

            // Act
            IHttpActionResult response = target.NewUser(user);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<User>));
            Assert.AreEqual(user, db.userRepo.GetById(2));
        }

        [TestMethod]
        public void DeleteUser_IdUserExistant_RetourneIdNull()
        {
            // Arrange
            int id = 1;

            // Act
            IHttpActionResult response = target.DeleteUser(id);

            // Assert
            Assert.IsNull(db.userRepo.GetById(id));
        }
    }
}
