﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Donnees.DAL;
using Donnees.Services;
using d75_david_et_jacky_backend.Controllers;
using d75_david_et_jacky_backend.Models;
using System.Collections.Generic;
using System.Web.Http;
using Effort;
using System.Web.Http.Results;
using System.Linq;

namespace d75_david_jacky_ky_trung_backend.Tests.Controllers
{
    [TestClass]
    public class LivreControllerTest
    {
        private VenteLivreDb context;
        private IUnitOfWork db;
        private LivreController target;

        [TestInitialize]
        public void Initialisation()
        {
            var connection = DbConnectionFactory.CreateTransient();
            context = new VenteLivreDb(connection);
            db = new UnitOfWork(context);
            
            target = new LivreController(db);

            db.matiereRepo.Add(new Matiere
            {
                Id = 1,
                Nom = "TestMatiere",
                Livres = new List<Livre>()
            });

            db.livreRepo.Add(new Livre
            {
                Titre = "TestTitre",
                ImageURL = "www.placeholder.com",
                Edition = "Edition LaFontaine",
                Auteur = "TestAuteur",
                ItemLivres = new List<ItemLivre>(),
                IdMatiere = 1
            });

            db.livreRepo.Add(new Livre
            {
                Titre = "TestTitre2",
                ImageURL = "22.png",
                Edition = "Edition Test",
                Auteur = "TestAuteurAAA",
                ItemLivres = new List<ItemLivre>(),
                IdMatiere = 1
            });
            db.Save();
        }

        [TestMethod]
        public void GetLivreParId_IdValide_RetourneIdLivre()
        {
            //Arrange
            int id = 1;
            Livre livreOriginal = db.livreRepo.GetById(id);

            //Act
            IHttpActionResult response = target.GetLivreById(id);
            var result = response as OkNegotiatedContentResult<Livre>;
            Livre livre = result.Content;

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Livre>));
            Assert.AreEqual(livreOriginal.Id, livre.Id);
        }

        [TestMethod]
        public void GetAllLivres_Appelee_RetourneListeLivres()
        {
            // Arrange
            int originalCount = db.livreRepo.Get().Count();

            // Act
            IHttpActionResult response = target.GetAllLivres();

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<IEnumerable<Livre>>));
            var result = response as OkNegotiatedContentResult<IEnumerable<Livre>>;
            IEnumerable<Livre> allLivres = result.Content;
            Assert.AreEqual(allLivres.Count(), originalCount);
        }

        [TestMethod]
        public void PutLivre_IdValide_RetourneIdLivre()
        {
            //Arrange
            int id = 1;
            Livre livre = db.livreRepo.GetById(id);
            string titreOriginal = livre.Titre;

            livre.Titre = "Titre2";
            //Act
            IHttpActionResult response = target.UpdateLivre(id, livre);

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Livre>));

            var result = response as OkNegotiatedContentResult<Livre>;
            Livre content = result.Content;
            Assert.AreEqual(id, content.Id);
            Assert.AreNotEqual(titreOriginal, content.Titre);
        }

        [TestMethod]
        public void AjoutLivre_LivreValide_RetourneIdLivreAjoute()
        {
            //Arrange
            Livre livre = new Livre
            {
                Titre = "TestTitre212",
                ImageURL = "test.png",
                Edition = "Edition LaFontaine",
                Auteur = "TestAuteur",
                ItemLivres = new List<ItemLivre>(),
                IdMatiere = 1
            };

            //Act
            IHttpActionResult response = target.NewLivre(livre);
            
            //Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Livre>));
            Assert.AreEqual(livre, db.livreRepo.GetById(3));
        }
        
        [TestMethod]
        public void DeleteLivre_IdLivreExistant_RetourneIdNull()
        {
            // Arrange
            int id = 1;

            // Act
            IHttpActionResult response = target.DeleteLivre(id);

            // Assert
            Assert.IsNull(db.userRepo.GetById(id));
        }
    }
}
