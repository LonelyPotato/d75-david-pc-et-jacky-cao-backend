﻿using d75_david_et_jacky_backend.Models;
using d75_david_jacky_ky_trung_backend.Controllers;
using Donnees.DAL;
using Donnees.Services;
using Effort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace d75_david_jacky_ky_trung_backend.Tests.Controllers
{
    [TestClass]
    public class CommandeControllerTest
    {
        private VenteLivreDb context;
        private IUnitOfWork db;
        private CommandeController target;

        [TestInitialize]
        public void TestInitialize()
        {
            var connection = DbConnectionFactory.CreateTransient();
            context = new VenteLivreDb(connection);
            db = new UnitOfWork(context);

            target = new CommandeController(db);

            db.userRepo.Add(new User
            {
                Id = 1,
                Nom = "oofnom",
                Prenom = "oofprenom",
                Username = "oofusername",
                Password = "oofpassword",
                Email = "oof@hotmail.com",
                ItemLivres = new List<ItemLivre>()
            });

            db.matiereRepo.Add(new Matiere
            {
                Id = 1,
                Nom = "matierenom",
                Livres = new List<Livre>()
            });

            db.livreRepo.Add(new Livre
            {
                Id = 1,
                Titre = "titrelivre",
                ImageURL = "www.lol.com",
                Edition = "Edition Cheneliere",
                Auteur = "auteur",
                ItemLivres = new List<ItemLivre>(),
                IdMatiere = 1
            });

            db.itemLivreRepo.Add(new ItemLivre
            {
                Id = 1,
                Titre = "titre",
                ImageURLs = "",
                Etat = Etat.Excellent,
                Prix = 15.00,
                Description = "Description",
                Statut = Statut.Libre,
                IdLivre = 1,
                IdVendeur = 1
            });

            db.commandeRepo.Add(new Commande
            {
                Id = 1,
                NumeroCommande = "TEST1234",
                IdItemAchete = 1,
                IdClient = 1
            });

            db.Save();
        }

        [TestMethod]
        public void GetCommandeById_IdValide_RetourneIdCommande()
        {
            //Arrange
            int id = 1;
            Commande commandeOriginale = db.commandeRepo.GetById(id);

            //Act
            IHttpActionResult response = target.GetCommandeById(id);

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Commande>));
            var result = response as OkNegotiatedContentResult<Commande>;
            Commande commande = result.Content;
            Assert.AreEqual(commandeOriginale.Id, commande.Id);
        }

        [TestMethod]
        public void GetCommandeByNoCommande_NoValide_RetourneNoCommande()
        {
            //Arrange
            string noCommande = "TEST1234";
            IEnumerable<Commande> commandeOriginale = db.commandeRepo.Get(x => x.NumeroCommande == noCommande);
            //Juste une commande sera retournée puisqu'un NoCommande est unique.

            //Act
            IHttpActionResult response = target.GetCommandeByNoCommande(noCommande);

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Commande>));
            var result = response as OkNegotiatedContentResult<Commande>;
            Commande commande = result.Content;
            Assert.AreEqual(commandeOriginale.First().NumeroCommande, commande.NumeroCommande);
        }
    }
}
