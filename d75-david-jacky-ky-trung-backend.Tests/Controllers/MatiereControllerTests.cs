using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using d75_david_et_jacky_backend.Controllers;
using d75_david_et_jacky_backend.Models;
using Donnees.DAL;
using Donnees.Services;
using Effort;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;

namespace d75_david_jacky_ky_trung_backend.Tests.Controllers
{
    [TestClass]
    public class MatiereControllerTests
    {
        private VenteLivreDb context;
        private IUnitOfWork db;
        private MatiereController target;

        [TestInitialize]
        public void TestInitialize()
        {
            var connection = DbConnectionFactory.CreateTransient();
            context = new VenteLivreDb(connection);
            db = new UnitOfWork(context);

            target = new MatiereController(db);

            db.matiereRepo.Add(new Matiere
            {
                Nom = "TestMatiere1",
            });

            db.matiereRepo.Add(new Matiere
            {
                Nom = "TestMatiere2",
            });

            db.Save();
        }

        [TestMethod]
        public void GetMatiereById_IdValide_RetourneIdMatiere()
        {
            // Arrange
            int id = 1;
            Matiere matiereOriginal = db.matiereRepo.GetById(id);

            // Act
            IHttpActionResult response = target.GetMatiereById(id);

            var result = response as OkNegotiatedContentResult<Matiere>;
            Matiere livre = result.Content;

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Matiere>));
            Assert.AreEqual(matiereOriginal.Id, livre.Id);
        }

        [TestMethod]
        public void GetAllMatieres_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            int originalCount = db.matiereRepo.Get().Count();

            // Act
            IHttpActionResult response = target.GetAllMatieres();

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<IEnumerable<Matiere>>));
            var result = response as OkNegotiatedContentResult<IEnumerable<Matiere>>;
            IEnumerable<Matiere> allMatieres = result.Content;
            Assert.AreEqual(allMatieres.Count(), originalCount);
        }

        [TestMethod]
        public void UpdateMatiere_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            int id = 1;
            Matiere matiere = db.matiereRepo.GetById(id);
            string nomOriginal = matiere.Nom;

            matiere.Nom = "AutreNomTest";

            // Act
            IHttpActionResult response = target.UpdateMatiere(id, matiere);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Matiere>));

            var result = response as OkNegotiatedContentResult<Matiere>;
            Matiere content = result.Content;
            Assert.AreEqual(id, content.Id);
            Assert.AreNotEqual(nomOriginal, content.Nom);
        }

        [TestMethod]
        public void NewMatiere_MatiereValide_RetourneIdMatiereAjoute()
        {
            // Arrange
            Matiere newMatiere = new Matiere
            {
                Nom = "NouvelleMatiere"
            };

            // Act
            IHttpActionResult response = target.NewMatiere(newMatiere);

            // Assert
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Matiere>));
            Assert.AreEqual(newMatiere, db.matiereRepo.GetById(3));
        }

        [TestMethod]
        public void DeleteMatiere_IdMatiereExistant_RetourneIdNull()
        {
            // Arrange
            int id = 1;

            // Act
            IHttpActionResult response = target.DeleteMatiere(id);


            // Assert
            Assert.IsNull(db.matiereRepo.GetById(id));
        }
    }
}
